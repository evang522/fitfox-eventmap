# Fitfox Events Map

## Deploy:
* Clone Repo `git clone https://evang522@bitbucket.org/evang522/fitfox-eventmap.git`
* `cd fitfox-eventmap`
* `npm install`
* Create .env file with values: (If it does not already exist)
  *  REACT_APP_API_URL=https://v2.fitfox.de/api
  *  REACT_APP_GOOGLE_API_KEY=**Private Key**
* `npm start`


## Under the Hood

Fitfox's v2 Api provides a few endpoints that this project makes use of to generate Geolocation points on a react-Google Map component. 

The Fitfox API contains a resource called `event`, which is a repeatable event (sponsored by a Gym or Group). The API also contains a resource called `eventoccurrence`. An `eventoccurrence` is an actual time and place instance of an `event`. These are two separate resources which have different endpoints (don't confuse the two!).


## Steps to produce Geolocation Results:
The goal is to display all of the cities in which events might occur so that users can look up events, if they exist, in their city.

#### Query the List of Available Cities
So first, we query the API for the available cities:

`GET v2.fitfox.de/api/city`

#### Query Event Api to find Events Associated with a City
Then, the app must query the Fitfox API to find if, indeed, there are any events associated with the city.

`GET v2.fitfox.de/api/events?criteria%5Bcity%5D=CITYNAME`

If so, the API returns back a list of all of the events that occur in their city.

#### Query the Event Occurrence Endpoint 

Since the `events` endpoint does not return `eventoccurrences` but `events`, once the events are retrieved, we must then query the Fitfox API for each event we receive back to find out what `eventoccurrences` exist for the event. 

`GET v2.fitfox.de/api/eventoccurence/:ID`

We then store this information in the Redux state as `state.events.eventOccurrences`.
#### Merge in GeoLocation Data

At the moment, the Fitfox API does not return any GeoLocation info about these eventoccurrences. But we are provided an Address, which we can make use of to query the Google Geocode Api to receive Latitude and Longitude for the event. We loop through each of the eventoccurrences in state and make a call to the below url to receive latitude and longitude information about the event.

`GET https://maps.googleapis.com/maps/api/geocode/json?address=ADDRESS+ZIPCODE`

Once we receive this information back from the Google GeoLocation API, we merge it into the information objects in our eventOccurrences array.  

Now we have a list of event Occurrences for the currently selected city with geolocation data stored. We may use this to display markers and infowindows on the Google Maps Component. 


