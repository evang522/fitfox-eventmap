//================================== Import Dependencies ====================>
import React from 'react';


//================================== Define Component ====================>


export default class MobileMenu extends React.Component {
  render() {

    return (
      <section className='mobile-menu-container'>
        <div className='mobile-menu-items'>
          <div className='logo-language-container ff-corporate-logo'>
            <img src='https://www.v2.fitfox.de/static/media/logo-white.04bb4a53.svg' alt='Fitfox logo'/>
            <div className='language-container-mobile'>
              EN
            </div>
            <div className='language-flag-container-mobile'>
              <img className='lang-img' src='https://www.v2.fitfox.de/static/media/en.1360e138.svg' alt='British Flag'/>
            </div>
          </div>
          <div className='mobile-navlinks'>
          <ul className='mobile-column-links'>
            <li>
              <a href='https://www.fitfox.de'>Home</a>
            </li>
            <li>
              <a  href='https://www.fitfox.de/events'>Events</a>
            </li>
            <li>
              <a href='https://www.fitfox.de/fitness-studios'>Studios</a>
            </li>
            <li>
              <a href='https://blog.fitfox.de'>Blog</a>
            </li>
          </ul>
          </div>
          <div className='mobile-actions'>
            <a href='https://www.fitfox.de/benutzer-registrieren/'>REGISTRIEREN</a>
            <a href='https://www.fitfox.de/login/'>LOGIN</a>
          </div>
        </div>
        <div onClick={() => this.props.hideMobileMenu()} className='mobile-back-button'>&larr;</div>
      </section>

    )

  }
}