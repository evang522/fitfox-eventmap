//================================== Import Dependencies ====================>
import React from 'react';
import {connect} from 'react-redux';
import './styles/CityList.css';
import {populateCityEvents} from '../state/actions/events.actions'

//================================== Define Component ====================>

export class CityList extends React.Component {

  onSetCityClick = (city) => {
    this.props.dispatch(populateCityEvents(city));
  }

  render() {


    const CityUnit = props =>  (
      <li onClick={() => props.onClick(props.cityName)} className='city-unit-container'>
        {props.cityName}
      </li>
    );

    const cities = this.props.cityList ? this.props.cityList.map((city,index) => <CityUnit key={index} onClick={this.onSetCityClick} cityName={city.name}/>) : '';



    return (
      <section className='city-list-container'>
        {/* <header>
        Verfügbare Städte
        </header> */}
        <ul className='city-list-ul'>
          {cities ? cities : ''}
        </ul>
      </section>

    )

  }

}

const mapStateToProps = state => ({
  cityList: state.cities.cityList
})

export default connect(mapStateToProps)(CityList);