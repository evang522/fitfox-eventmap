//================================== Import Dependencies ====================>
import React from 'react';
import { withScriptjs, GoogleMap, Marker, withGoogleMap, InfoWindow} from 'react-google-maps';
import {connect} from 'react-redux';
import {setCurrentInfoWindow, populateCityEvents, setMapCenter, setMapZoom} from '../state/actions/events.actions';
import './styles/Map.css';
import moment from 'moment';
import { showCitiesOff } from '../state/actions/cities.actions';
import mapStyles from '../utils/mapStyles';
import {GOOGLE_API_KEY} from '../config'
//================================== Define MAP Component ====================>



// Imports Google Map and creates a base Google Maps Component which our Map component will display with further configuration information. 
export const MapBaseComponent = withScriptjs(withGoogleMap(props =>  {
  return (<GoogleMap  
  
    defaultOptions={{ styles: mapStyles,gestureHandling: 'cooperative' }}
    onClick={() => props.closeInfoWindows()}
    zoom={props.mapZoom || 6}
    defaultZoom={6}
    defaultCenter={{lat:50.978504, lng: 10.31207}}
    center={props.mapCenter || {lat:50.978504, lng: 10.31207}}
    >
    {props.eventMarkers ? props.eventMarkers : ''}
    {props.showCities && props.cityMarkers ? props.cityMarkers : ''}
  </GoogleMap>)
  }
))





//================================== Define Map Wrapper Component ====================>
export class Map extends React.Component {

  // The reason this function is called redundantly is to force a rerender from react. This allows InfoWindows to be opened a second time after they have been closed
  setCurrentInfoWindow = index => {
    this.props.dispatch(setCurrentInfoWindow(null));
    this.props.dispatch(setCurrentInfoWindow(index));
  }

  // When a city marker is clicked, a chain of API calls will initiate that will eventually store information about event occurrences in state, which will then be displayed on the map. 
  // 'Show Cities' in state is set to false so that only the event occurrences in a specific city are displayed. 
  handleCityClick = (cityName,cityCoordinates) => {
    this.props.dispatch(populateCityEvents(cityName))
    this.props.dispatch(showCitiesOff());
  }
  // Handles event marker click and opens an infoWindow. Zooms map further to more clearly see event context, and sets the map center to the event coordinates. 
  handleEventClick = (index, coordinates) => {
    this.setCurrentInfoWindow(index)
    this.props.dispatch(setMapZoom(13));
    this.props.dispatch(setMapCenter(coordinates));
  }

  render() {

    // Loop over event Occurrences in state and generate an array of Marker components which will be displayed on the map. 
    const eventMarkers = this.props.events && this.props.events.length && this.props.events[0].locationInfo ? this.props.events.map((event,index) => {
    return (
    <Marker 
      icon={'src/img/marker.png'}
      onClick={() => this.handleEventClick(index, {lat:Number(event.locationInfo.coordinates.lat), lng:Number(event.locationInfo.coordinates.lng)})}
      key={index} 
      title={event.parentEventName}
      position={{lat:Number(event.locationInfo.coordinates.lat), lng: Number(event.locationInfo.coordinates.lng)}}
      >
      {(this.props.currentInfoWindow || this.props.currentInfoWindow ===0) && this.props.currentInfoWindow === index ? 
      <InfoWindow>
        <div className='info-window-container'>
          <div className='info-window-title'><b>{event.parentEventName}</b></div>
          <div className='info-window-time'><b>{moment(event.startDateTime).format("dddd, MMMM Do, h:mm a")}</b></div>
          <div className='info-window-description'><a target="_blank" href={`https://www.fitfox.de/events/${event.city.name}/${event.parentEventName}/${event.id}`} > See More</a></div>
          <div className='info-window-pic'><img src={event.imageURL || "https://s3-eu-west-1.amazonaws.com/fitfox-events-production/lauftreff-mu%CC%88nchen-fitfox-eins.jpg"} alt={'Event'}/></div>
        </div>
      </InfoWindow>
       : ''}
    </Marker>
    )}) : '';

    //Loops over cityList in state to produce an array of Marker components and displays city markers on the Map. 
    const cityMarkers = this.props.cityList && this.props.cityList.length ? this.props.cityList.map((city,index) => {
      return (
          <Marker
          icon={'src/img/marker.png'}
          onClick={() => this.handleCityClick(city.name)}
          position={{lat: Number(city.lat), lng: Number(city.lng)}}
          key={index}
          title={city.name}
          />
        )
      
    }) : '';
    
    return(
        <MapBaseComponent
        showCities={this.props.showCities}
        closeInfoWindows={() => this.props.dispatch(setCurrentInfoWindow(null))}
        mapZoom={this.props.mapZoom}
        mapCenter={this.props.mapCenter}
        cityMarkers={cityMarkers ? cityMarkers : ''}
        eventMarkers={eventMarkers ? eventMarkers : ''}
        isMarkerShown={true}
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${GOOGLE_API_KEY}`}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ width:`90%`, 'margin':`0 auto`, 'marginTop':'1em' , 'marginBottom':'1em'}} />}
        mapElement={<div style={{ height: `100%` }} />}
        />
    );
  }

}

const mapStateToProps = state => ({
  events: state.events.eventOccurrenceList,
  currentInfoWindow: state.events.currentInfoWindow,
  mapCenter: state.events.mapCenter,
  mapZoom: state.events.mapZoom,
  showCities: state.cities.showCities,
  cityList: state.cities.cityList
})

export default connect(mapStateToProps)(Map)