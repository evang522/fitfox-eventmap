//================================== Import Dependencies ====================>
import React, {Component} from 'react';
import './styles/AppHeader.css';
import MobileMenu from './MobileMenu';

//================================== Define Component ====================>
export default class AppHeader extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileMenuShown: false
    }

  }


  hideMobileMenu = () => {
    this.setState({
      mobileMenuShown:false
    })
  }

  render() {

    return (
      <header className='main-app-header-container'>
        {this.state.mobileMenuShown ? <MobileMenu hideMobileMenu={this.hideMobileMenu}/> : '' }
        <a 
        onClick = { () => {
          this.setState({
            mobileMenuShown:true
          })
        }}
        className="navbar-toggle"
        >
          <div>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </div>
        </a>
        <div className='ff-corporate-logo'>
          <a href='http://www.fitfox.de'>
          <img
            src='https://www.v2.fitfox.de/static/media/logo-white.04bb4a53.svg'
            alt='Fitfox GmbH Logo'/>
          </a>
        </div>
        <div className='app-header-navlinks'>
          <ul className='navlinks-ul'>
            <li>
              <a href='https://www.fitfox.de'>Home</a>
            </li>
            <li>
              <a className='current-page' href='https://www.fitfox.de/events'>Events</a>
            </li>
            <li>
              <a href='https://www.fitfox.de/fitness-studios'>Studios</a>
            </li>
            <li>
              <a href='https://blog.fitfox.de'>Blog</a>
            </li>
          </ul>
        </div>
        <div className='app-header-right-navlinks'>
          <ul>
            <li className='registrieren'>
              <a href='https://www.fitfox.de/registrieren/'>Registrieren</a>
            </li>
            <li className='login-link'>
              <a href='https://www.fitfox.de/login'>LOGIN</a>
            </li>
            <li className='language-box'>

              <div className='language-signifier'>
                EN
              </div>
              <div className='language-flag'>
                <img src='https://www.fitfox.de/build/img/en.svg' alt='Language flag'/>
              </div>
            </li>
          </ul>
        </div>
      </header>

    )

  }
}