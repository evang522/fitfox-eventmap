//================================== Import Dependencies ====================>
import React, {Component} from 'react';
import {fetchCityList, showCitiesOn} from '../state/actions/cities.actions';
import {connect} from 'react-redux';
import './styles/CityChooser.css'
import { clearEventdata, setMapCenter, resetMapZoom, setCurrentInfoWindow} from '../state/actions/events.actions';


//================================== Define Component ====================>
export class CityChooser extends Component {


  componentDidMount() {
    this.props.dispatch(fetchCityList());
  }
  
  onClick = () => {
    if (!this.props.showCities) {
      this.props.dispatch(showCitiesOn());
    }
    this.props.dispatch(clearEventdata())
    this.props.dispatch(setMapCenter(null))
    this.props.dispatch(resetMapZoom());
    this.props.dispatch(setCurrentInfoWindow(null));
  }

  render() {

    

    return (
    <section className='city-choose-expander-container'>
        <div className='city-chooser-header'>
          <h3>Suchen Sie nach Veranstaltungen in Ihrer Nähe...</h3>
        </div>
      {this.props.showCities ?
        this.props.statusMessage ? 
        <div className='choose-city-greet'>{this.props.statusMessage}</div>
          :
        <div className='choose-city-greet'>Wähle eine Stadt</div>
          :
        <button onClick={() => this.onClick()} className='pointercursor city-chooser-expander'>Zeige alle Städte</button>
      }
    </section>


    )
  }
}

const mapStateToProps = state => ({
    showCities: state.cities.showCities,
    statusMessage: state.general.statusMessage
})

export default connect(mapStateToProps)(CityChooser);