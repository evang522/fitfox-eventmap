//================================== Import Dependencies ====================>
import React, { Component } from 'react';
import './App.css';
import AppHeader from './components/AppHeader';
import CityChooser from './components/CityChooser';
import Map from './components/Map';
import {connect} from 'react-redux';
import Spinner from './components/Spinner';
import CityList from './components/CityList';

//================================== Define Component ====================>


export class App extends Component {
  render() {
    return (
      <div className="App">
        {this.props.loading ? <Spinner/> : ''}
        <AppHeader/>
        <CityChooser/>
        <div className='map-citylist-flex-container'>
          <CityList/>
          <Map eventLocations={this.props.eventLocations ? this.props.eventLocations: ''}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loading:state.general.loading
})

export default connect(mapStateToProps)(App);
