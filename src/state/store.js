//================================== Import Dependencies ====================>
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

//Reducers
import generalReducer from './reducers/general.reducer';
import citiesReducer from './reducers/cities.reducers';
import eventsReducer from './reducers/events.reducers';


export const rootReducer = combineReducers({
  general:generalReducer,
  cities:citiesReducer,
  events:eventsReducer
});

export default createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk)
);