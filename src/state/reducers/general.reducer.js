//================================== Import Dependencies ====================>
import {SET_LOADING, SET_STATUS_MESSAGE, CLEAR_LOADING} from '../actions/general.actions';

//================================== Define Initial State ====================>


const initialState = {
  loading:false,
  error: {
    info: null,
    bool:false
  },
  statusMessage:null
}


const generalReducer = (state=initialState, action) => {

  switch(action.type) {

    case SET_STATUS_MESSAGE:
    return {
      ...state,
      statusMessage:action.message
    }

    case SET_LOADING:
      return {
        ...state,
        loading:true
      }
    

    case CLEAR_LOADING:
    return {
      ...state,
      loading:false
    }
    //=================>



    //=================>
    default:
    return state;
  }
}

export default generalReducer;