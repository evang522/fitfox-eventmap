//================================== Import Dependencies ====================>
import {STORE_CITY_EVENTS, SET_MAP_CENTER, SET_MAP_ZOOM, ADD_GEOLOCATION_DATA, CLEAR_EVENT_DATA, STORE_EVENT_OCCURRENCES, RESET_MAP_ZOOM, SET_CURRENT_INFOWINDOW} from '../actions/events.actions';
//================================== Define Initial State ====================>

const initialState = {
  eventList:null,
  eventOccurrenceList:[],
  currentInfoWindow:null,
  mapCenter: {lat:50.978504, lng: 10.31207},
  mapZoom:null

}


const eventsReducer = (state=initialState, action) => {

  switch(action.type) {


    //Controls Google Map Component Zoom level. This value changes depending on whether the user is looking at an overview of all available cities, events available in a specific city, or a single event.
    case SET_MAP_ZOOM:
    return {
      ...state,
      mapZoom:action.zoomLevel
    }

    // InfoWindows pop up on clicking map markers to display a picture and information/links for an event. This reducer will set a prop in state informing individual infowindows which infowindow currently ought to be open.
    case SET_CURRENT_INFOWINDOW:
    return {
      ...state, 
      currentInfoWindow:action.index
    }



    //==========================>

    //Sets map zoom to null, whereupon the map will by default render a predetermined zoom level.
    case RESET_MAP_ZOOM:
    return {
      ...state,
      mapZoom:null
    }

    //==========================>

    // Sets map's latitude and longitude coordinates for the center of the display. If this value (mapCenter) is null, the map has a default set of coordinates that it will fall back to.
    case SET_MAP_CENTER:
      return {
        ...state,
        mapCenter: action.centerObj,
      }


    //===========================>

    // Receives an object containing an event occurence's latitude and longitude information and merges it in with the object within the event Occurrence in State.
    case ADD_GEOLOCATION_DATA:
    return {
      ...state,
      eventOccurrenceList: [
        ...state.eventOccurrenceList.filter(occurrence => occurrence.id !== action.data.id),
        {
          ...state.eventOccurrenceList.find(occurrence => occurrence.id === action.data.id),
          locationInfo:action.data
        }
      ]
    }

    //============================>

    // Clears all event. Since all data pertaining to different events is city-specific, on clicking a new city, the previous information needs to be cleared out. This reducer is run every time a new city is chosen.
    case CLEAR_EVENT_DATA:
    return {
      ...state,
      eventList:null,
      eventOccurrenceList:[]
    }

    //====================>

    // Stores event occurences received from the API by adding them into the existing array of event Occurrences in state.
    case STORE_EVENT_OCCURRENCES:
    return {
      ...state,
      eventOccurrenceList: [
        ...state.eventOccurrenceList,
        ...action.eventOccurrences
      ]
    }

    //====================>
    
    // Stores event containers related to cities in the array in state, upon receipt from the API call.
    case STORE_CITY_EVENTS:
    return {
      ...state,
      eventList:action.events
    }
    
    //===================>
    default: 
    return state;



  }

}

export default eventsReducer;