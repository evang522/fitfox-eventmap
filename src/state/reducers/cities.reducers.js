//================================== Import Dependencies ====================>
import {STORE_CITY_LIST, SET_CURRENT_CITY, SHOW_CITIES_OFF, SHOW_CITIES_ON} from '../actions/cities.actions';

//================================== Define Initial State ====================>

const initialState = {
  cityList:null,
  currentCity:null,
  showCities:true
}


const citiesReducer = (state=initialState, action) => {

  switch(action.type) {

  // Informs app to not display cities from cityList in state as Map Markers
  case SHOW_CITIES_OFF:
    return {
      ...state,
      showCities: false
    }

  //
  //====================>

    // Informs app to display cities from cityList in state as Map Markers
    case SHOW_CITIES_ON:
    return {
      ...state,
      showCities:true
    }

    //====================>

    // Receives list of available cities as a result of async call to API and stores them in Redux State
    case STORE_CITY_LIST:
    return {
      ...state,
      cityList:action.cityList
    }

    //===================>

    // Sets the current city prop in state. This informs the State which city is currently in focus.
    case SET_CURRENT_CITY:
    return {
      ...state,
      currentCity:action.city
    }
    
    //===================>
    default: 
    return state;
  }

}

export default citiesReducer;