//================================== Import Dependencies ====================>



//================================== Sync Actions ====================>
export const SET_LOADING = 'SET_LOADING';
export const setLoading = () => ({
  type:SET_LOADING
})

export const SET_STATUS_MESSAGE = 'SET_STATUS_MESSAGE';
export const setStatusMessage = message => ({
  type:SET_STATUS_MESSAGE,
  message
})

export const CLEAR_LOADING = 'CLEAR_LOADING';
export const clearLoading = () => ({
  type:CLEAR_LOADING
})

//================================== Async Actions ====================>
