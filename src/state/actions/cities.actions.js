//================================== Import Dependencies ====================>
import axios from 'axios';
import {API_URL} from '../../config';
import {GOOGLE_API_KEY} from '../../config';
const geoCodeURL = 'https://maps.googleapis.com/maps/api/geocode/json?address='


//================================== Sync Actions ====================>
export const STORE_CITY_LIST = 'STORE_CITY_LIST';
export const storeCityList = cityList => ({
  type:STORE_CITY_LIST,
  cityList
})


export const SET_CURRENT_CITY = 'SET_CURRENT_CITY';
export const setCurrentCity = city => ({
  type:SET_CURRENT_CITY,
  city
})


export const SHOW_CITIES_ON = 'SHOW_CITIES_ON';
export const showCitiesOn = () => ({
  type: SHOW_CITIES_ON
})

export const SHOW_CITIES_OFF = 'SHOW_CITIES_OFF';
export const showCitiesOff = () => ({
  type: SHOW_CITIES_OFF
})

//================================== Async Actions ====================>


export const fetchCityList = () => dispatch => {

  axios({
    'url':`${API_URL}/city?limit=100`,
    'method':'GET',
  })
  .then(response => {
    const cityList = response.data._embedded.items.map(item => Object.assign({},item));
    let counter=0;

    return new Promise((resolve,reject) => {
      cityList.forEach((city,index) => {
        axios({
          'url':`${geoCodeURL}${city.name}+Europe&key=${GOOGLE_API_KEY}`,
          'method':'GET',
        })
        .then(geoResponse => {
          cityList[index]['lat'] = Number(geoResponse.data.results[0].geometry.location.lat);
          cityList[index]['lng'] = Number(geoResponse.data.results[0].geometry.location.lng);
          counter++
          if (counter === cityList.length) {
            resolve(cityList);
          }
        })
      })
    })
  })
  .then(cityList => {
    dispatch(storeCityList(cityList));
  })
  .catch(err => {
    console.log(err);
  })
}



