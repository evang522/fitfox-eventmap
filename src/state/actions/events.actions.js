//================================== Import Dependencies ====================>
import axios from 'axios';
import {API_URL} from '../../config';
import {setCurrentCity, showCitiesOff, showCitiesOn} from './cities.actions';
import {GOOGLE_API_KEY} from '../../config';
import {setStatusMessage, setLoading, clearLoading} from './general.actions';
const geoCodeURL = 'https://maps.googleapis.com/maps/api/geocode/json?address='
//================================== Sync Actions ====================>

export const STORE_CITY_EVENTS = 'STORE_CITY_EVENTS';
export const storeCityEvents = events => ({
  type:STORE_CITY_EVENTS,
  events
})

export const SET_CURRENT_INFOWINDOW = 'SET_CURRENT_INFOWINDOW';
export const setCurrentInfoWindow = index => ({
  type: SET_CURRENT_INFOWINDOW,
  index
})

export const STORE_EVENT_OCCURRENCES = 'STORE_EVENT_OCCURRENCES';
export const storeEventOccurences = eventOccurrences => ({
  type: STORE_EVENT_OCCURRENCES,
  eventOccurrences
})

export const CLEAR_EVENT_DATA = 'CLEAR_EVENT_DATA';
export const clearEventdata = () => ({
  type:CLEAR_EVENT_DATA
})


export const ADD_GEOLOCATION_DATA = 'ADD_GEOLOCATION_DATA';
export const addGeolocationdata = data => ({
  type:ADD_GEOLOCATION_DATA,
  data
})

export const SET_MAP_CENTER = 'SET_MAP_CENTER';
export const setMapCenter = centerObj => ({
  type:SET_MAP_CENTER,
  centerObj
})


export const RESET_MAP_ZOOM = 'RESET_MAP_ZOOM';
export const resetMapZoom = () => ({
  type:RESET_MAP_ZOOM,
})

export const SET_MAP_ZOOM = 'SET_MAP_ZOOM';
export const setMapZoom = zoomLevel => ({
  type:SET_MAP_ZOOM,
  zoomLevel
})





//================================== Async Actions ====================>


export const populateCityEvents = city => (dispatch,getState) => {
  dispatch(showCitiesOff());
  dispatch(setLoading());
  dispatch(clearEventdata());
  dispatch(setCurrentCity(city));
    axios({
      'url':`${API_URL}/event?criteria%5Bcity%5D=${city}`,
      'method':'GET'
    })
    .then(response => {

      dispatch(storeCityEvents(response.data._embedded.items))

      return new Promise((resolve,reject) => {
        const arrOfEvents = getState().events.eventList;

        let counter = 0;
        if (arrOfEvents.length) {
          dispatch(setMapZoom(10));
          arrOfEvents.forEach(event => {
            axios({
              'url':`${API_URL}/event/${event.id}`,
              'method':'GET'
            })
            .then(response => {
              const eventOccurrences = response.data.eventOccurrences.map(occurrence => Object.assign({}, occurrence, {parentEventName:event.name, imageURL:event.imageURL} ))
              counter++;
              dispatch(storeEventOccurences(eventOccurrences))
              if (counter === arrOfEvents.length) {
                resolve();
              }
            })
          })
          dispatch(setStatusMessage(null));
        } else {
          dispatch(setStatusMessage(`Keine Veranstaltungen in ${getState().cities.currentCity} gefunden.`));
          dispatch(setMapCenter(null));
          dispatch(resetMapZoom())
          dispatch(showCitiesOn());
          dispatch(clearLoading());
        }

      })
    })
    .then(() => {

        const eventOccurrences = getState().events.eventOccurrenceList;

        eventOccurrences.forEach(event => {
          return axios({
            'url':`${geoCodeURL}${event.street}+${event.zipCode}&key=${GOOGLE_API_KEY}`,
            method:'GET'
          })
          .then(response => {
            const eventLocationInfo = {
              id:event.id,
              coordinates: response.data.results[0].geometry.location
            }
            dispatch(addGeolocationdata(eventLocationInfo));
            dispatch(setMapCenter(eventLocationInfo.coordinates))
          })  

        })

        dispatch(clearLoading());
  });


}


