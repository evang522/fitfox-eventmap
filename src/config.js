export const API_URL = process.env.REACT_APP_API_URL || 'https://v2.fitfox.de/api'
export const GOOGLE_API_KEY = process.env.REACT_APP_GOOGLE_API_KEY