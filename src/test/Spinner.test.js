//================================== Import dependencies ====================>
import {mount, shallow} from 'enzyme';
import React from 'react';
import Spinner from '../components/Spinner';


//================================== Test Spinner Component ====================>

describe('<Spinner/>', () => {
  it('Should render without crashing', () => {
    let spinner = shallow(<Spinner/>);
    expect(spinner.find('.sk-fading-circle').children().length).toBe(12);
  })
})