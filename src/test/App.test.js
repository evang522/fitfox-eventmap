//================================== Import Dependencies ====================>
import {shallow, mount} from 'enzyme';
import React from 'react';
import {App} from '../App';

//================================== Test App.js ====================>

describe('<App/>', () => {
  it('Should render without crashing', () => {
    let app = shallow(<App loading={true}/>)
    expect(app.children().length).toBe(4);
  })

  it('Should not render the Loading component when no loading prop present', () => {
    let app = shallow(<App/>);
    expect(app.children().length).toBe(3);
  })
})