//================================== Import Dependencies ====================>
import React from 'react';
import {CityList} from '../components/CityList';
import Enzyme, {shallow, mount} from 'enzyme';

//================================== City List Tests ====================>
describe('Renders without crashing', () => {
  let cityList = mount(<CityList/>);
  expect(cityList.children().length).toEqual(1);
})



describe('<CityList/>', () => {
  it('Should render the correct text depending on the supplies props', () => {
    let cityList = mount(<CityList cityList={[{'name':'Berlin'},{'name':'Hamburg'}]}/>);
    expect(cityList.find('.city-unit-container').at(0).text()).toEqual('Berlin');
  })
})