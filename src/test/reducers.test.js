//================================== Import Dependencies ====================>
import React from 'react';
import store, { rootReducer } from '../state/store';
import Enzyme, {shallow, mount} from 'enzyme';
import * as cityActions from '../state/actions/cities.actions';
import * as eventActions from '../state/actions/events.actions';
import * as generalActions from '../state/actions/general.actions';
//================================== Reducer Tests ====================>

const initialState = {
  "cities": {
    "cityList": null,
    "currentCity": null,
    "showCities": true
  },
  "events": {
    "currentInfoWindow": null,
    "eventList": null,
    "eventOccurrenceList": [],
    "mapCenter": {
      "lat": 50.978504,
      "lng": 10.31207
    },
    "mapZoom": null
  },
  "general": {
    "error": {
      "bool": false,
      "info": null
    },
    "loading": false,
    "statusMessage": null
  }
}

//=========================== General Reducers Inital State Test=============>



describe('Reducer Tests', () => {
  it('Should return an initial State', () => {
    expect(Object.keys(rootReducer({}, {}))).toEqual(['general','cities','events']);
  })
})


//================================== Test City Reducers ====================>


describe('City Reducer Tests', () => {
  it('Should populate the City list', () => {
    const storeCityAction = {
      type:cityActions.STORE_CITY_LIST,
      cityList: ['1','2','3','4']
    }
    expect(rootReducer({}, storeCityAction).cities.cityList).toEqual(['1','2','3','4']);
  })


  it('Should Set the current City', () => {
    const setCurrentCity = {
      type:cityActions.SET_CURRENT_CITY,
      city: 'Hamburg'
    }
    expect(rootReducer({},setCurrentCity).cities.currentCity).toEqual('Hamburg');
  })

  it('Should set show cities to false', () => {
    const showCitiesOff = {
      type:cityActions.SHOW_CITIES_OFF
    }
    expect(rootReducer({},showCitiesOff).cities.showCities).toBe(false);
  })

})



//================================== Test Event Reducers ====================>


describe('Event Reducer Tests', () => {
  it('Should set the current InfoWindow to be shown', () => {
    const setInfoWindow = {
      type:eventActions.SET_CURRENT_INFOWINDOW,
      index:1
    }
    expect(rootReducer({}, setInfoWindow).events.currentInfoWindow).toEqual(1);
  })

  it('Should set the Map Zoom to null', () => {
    const setMapZoom = {
      type:eventActions.RESET_MAP_ZOOM,

    }
    expect(rootReducer({},setMapZoom).events.mapZoom).toEqual(null);
  })

  it('Should set the map Center and zoom to 10', () => {
    const centerObj=  {lat:10, long:10};
    const setMapCenter  = {
      type:eventActions.SET_MAP_CENTER,
      centerObj
    }

    expect(rootReducer({}, setMapCenter).events.mapCenter).toBe(centerObj);
  })

  it('Should Clear Event Data', () => {
    const mockState = {
      ...initialState,
      'events': {
      ...initialState.events,
      eventList:['Redux','Test','Wohoo!'], 
      eventOccurrenceList: ['list','Of','Occurences']
      }
    };
    const clearEventDataAction = {
      type:eventActions.CLEAR_EVENT_DATA
    }
    expect(rootReducer(mockState,{}).events.eventList).toEqual(['Redux','Test','Wohoo!']);

    expect(rootReducer(mockState, clearEventDataAction).events.eventList).toEqual(null);
  })


  it('Should Store Event Occurences', () => {
    const storeEventOccurences = {
      type:eventActions.STORE_EVENT_OCCURRENCES,
      eventOccurrences: [{'id':'12345','name':'SportsCheck Run Cologne'}, {'id':'123456', 'name':'Boom Bang Nämaste'}]
    }

    expect(rootReducer({}, storeEventOccurences).events.eventOccurrenceList).toEqual([{'id':'12345','name':'SportsCheck Run Cologne'}, {'id':'123456', 'name':'Boom Bang Nämaste'}])
  })

  it('Should Store City Events', () => {
    const storeCityEvents = {
      type: eventActions.STORE_CITY_EVENTS,
      events: ['1','2','3']
    }
    expect(rootReducer({}, storeCityEvents).events.eventList).toEqual(['1','2','3']);
  })

})



//================================== Test General Reducer ====================>


describe('General Reducer Tests', () => {
  it('Set a Status Message', () => {
    const setStatus = {
      type:generalActions.SET_STATUS_MESSAGE,
      message:'THIS IS A DRILL!'
    }

    expect(rootReducer({}, setStatus).general.statusMessage).toEqual('THIS IS A DRILL!');
  })

  it('Should set Loading to true', () => {
    const setLoading = {
      type:generalActions.SET_LOADING
    }
    expect(rootReducer({}, setLoading).general.loading).toBe(true);
  })

  it('Should Clear Loading', () => {
    const clearLoading = {
      type:generalActions.CLEAR_LOADING
    }

    expect(rootReducer({}, clearLoading).general.loading).toBe(false);
  })

  it('')
})