//================================== Import Dependencies ====================>
import React from 'react';
import {shallow} from 'enzyme';
import {Map} from '../components/Map';

//================================== Test Map.js ====================>

describe('<Map/>', () => {
  it('Should render without crashing', () => {

    let map = shallow(<Map/>);
    expect(map.name()).toEqual('withScriptjs(withGoogleMap(Component))');
  })
})
