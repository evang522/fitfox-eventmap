//================================== Import Dependencies ====================>
import React from 'react';
import MobileMenu from '../components/MobileMenu';
import Enzyme, {shallow, mount} from 'enzyme';

//================================== Define Tests ====================>

describe('<MobileMenu/>', () => {
  it('Smoketest for Mobile Menu', () => {
    let mobileMenu = shallow(<MobileMenu/>);
    expect(mobileMenu).toHaveLength(1);

  })

  it('Ensure Component renders text', () => {
    let mobileMenu = mount(<MobileMenu/>)
    expect(mobileMenu.find('.language-container-mobile').text()).toBe('EN');
  })
})