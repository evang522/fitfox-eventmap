//================================== Import Dependencies ====================>
import React from 'react';
import AppHeader from '../components/AppHeader';
import Enzyme, {shallow, mount} from 'enzyme';

//================================== Define Tests ====================>

describe('<AppHeader/>', () => {
  it('Smoke Test for AppHeader', () => {
    let appHeader = shallow(<AppHeader/>);
    expect(appHeader).toHaveLength(1);
  })

  it('Should mount with text and construct state', () => {
    let appHeaderMount = shallow(<AppHeader/>)
    expect(appHeaderMount.find('.registrieren').text()).toBe('Registrieren');
    expect(appHeaderMount.state().mobileMenuShown).toBe(false);
  });

  it('Clicking the menu button should reveal the mobile menu', () => {
    let appHeaderShallow = shallow(<AppHeader/>)
    appHeaderShallow.find('.navbar-toggle').simulate('click');
    expect(appHeaderShallow.state()).toEqual({
      mobileMenuShown:true
    })
  })
})



