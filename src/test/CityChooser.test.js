//================================== Import Dependencies ====================>
import React from 'react';
import {CityChooser} from '../components/CityChooser';
import Enzyme, {shallow, mount} from 'enzyme';
//================================== City Chooser Tests ====================>


describe('<CityChooser/>', () => {
  it('Should render without crashing', () => {
    let cityChooser = shallow(<CityChooser showCities={true}/>)
    expect(cityChooser.find('.choose-city-greet').text()).toBe('Wähle eine Stadt');
    let cityChooser1 = shallow(<CityChooser showCities={false}/>);
    expect(cityChooser1.find('.city-chooser-expander').text()).toBe('Zeige alle Städte');
  })
})